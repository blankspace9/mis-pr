# mis-pr

## Getting started
* Запуск приложения
`go run cmd/mis/main.go --config=./config/local.yaml`
* Запуск мигратора
`go run cmd/migrator/main.go --migrations-path=./migrations`
* Сборка
`go build -o cmd/mis/mis cmd/mis/main.go`
* Запуск сервиса на удаленном сервере
`systemctl start mis-pr`