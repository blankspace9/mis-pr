package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
)

func (s *Storage) GetDoctorByEmail(ctx context.Context, email string) (models.Doctor, error) {
	const op = "storage.postgres.GetDoctorByEmail"

	stmt, err := s.db.Prepare(
		`SELECT u.id, u.email, u.pass_hash, u.first_name, u.last_name, u.patronymic, u.phone_number, u.created_at, d.specialty, d.license FROM users u
		JOIN doctors d ON u.id = d.user_id
		WHERE u.email=$1`)
	if err != nil {
		return models.Doctor{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, email)

	var doctor models.Doctor
	err = row.Scan(&doctor.ID, &doctor.Email, &doctor.Password, &doctor.Firstname, &doctor.Lastname, &doctor.Patronymic, &doctor.PhoneNumber, &doctor.CreatedAt, &doctor.Specialty, &doctor.License)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Doctor{}, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return models.Doctor{}, fmt.Errorf("%s: %w", op, err)
	}

	return doctor, nil
}

func (s *Storage) GetDoctorById(ctx context.Context, id int64) (models.Doctor, error) {
	const op = "storage.postgres.GetDoctorById"

	stmt, err := s.db.Prepare(
		`SELECT u.id, u.email, u.pass_hash, u.first_name, u.last_name, u.patronymic, u.phone_number, u.created_at, d.specialty, d.license FROM users u
		JOIN doctors d ON u.id = d.user_id
		WHERE u.id=$1`)
	if err != nil {
		return models.Doctor{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, id)

	var doctor models.Doctor
	err = row.Scan(&doctor.ID, &doctor.Email, &doctor.Password, &doctor.Firstname, &doctor.Lastname, &doctor.Patronymic, &doctor.PhoneNumber, &doctor.CreatedAt, &doctor.Specialty, &doctor.License)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Doctor{}, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return models.Doctor{}, fmt.Errorf("%s: %w", op, err)
	}

	return doctor, nil
}

func (s *Storage) GetDoctorPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error) {
	const op = "storage.postgres.GetDoctorPrescriptions"

	stmt, err := s.db.Prepare("SELECT id, patient_id, doctor_id, date_of_issue, duration_days, title, dosage_mg, way_to_use, created_at FROM prescriptions WHERE doctor_id=$1")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var prescriptions []models.Prescription
	for rows.Next() {
		var prescription models.Prescription
		err = rows.Scan(&prescription.ID, &prescription.PatientID, &prescription.DoctorID, &prescription.DateOfIssue, &prescription.DurationDays, &prescription.Title, &prescription.DosageMg, &prescription.WayToUse, &prescription.CreatedAt)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		prescriptions = append(prescriptions, prescription)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return prescriptions, nil
}

func (s *Storage) GetDoctorPatients(ctx context.Context, id int64) ([]models.User, error) {
	const op = "storage.postgres.GetDoctorPatients"

	stmt, err := s.db.Prepare(
		`SELECT DISTINCT p.patient_id, u.email, u.pass_hash, u.first_name, u.last_name, u.patronymic, u.phone_number, u.created_at FROM prescriptions p
		JOIN users u ON p.doctor_id = u.id
		WHERE p.doctor_id=$1`)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var patients []models.User
	for rows.Next() {
		var patient models.User
		err = rows.Scan(&patient.ID, &patient.Email, &patient.Password, &patient.Firstname, &patient.Lastname, &patient.Patronymic, &patient.PhoneNumber, &patient.CreatedAt)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		patients = append(patients, patient)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return patients, nil
}

func (s *Storage) DeleteDoctor(ctx context.Context, id int64) error {
	const op = "storage.postgres.DeleteDoctor"

	stmt, err := s.db.Prepare("DELETE FROM users WHERE id=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}
