package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
)

func (s *Storage) CreateSession(ctx context.Context, patientId, doctorId int64) (int64, error) {
	const op = "storage.postgres.CreateSession"

	stmt, err := s.db.Prepare("INSERT INTO sessions (patient_id, doctor_id, created_at) VALUES ($1, $2, $3) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, patientId, doctorId, time.Now())

	var sessionId int64
	err = row.Scan(&sessionId)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrSessionExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return sessionId, nil
}

func (s *Storage) GetSessionByMembers(ctx context.Context, patientId, doctorId int64) (models.Session, error) {
	const op = "storage.postgres.Get"

	stmt, err := s.db.Prepare("SELECT id, patient_id, doctor_id, created_at FROM sessions WHERE patient_id=$1 AND doctor_id=$2")
	if err != nil {
		return models.Session{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, patientId, doctorId)

	var session models.Session
	err = row.Scan(&session.ID, &session.PatientID, &session.DoctorID, &session.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Session{}, fmt.Errorf("%s: %w", op, storage.ErrSessionNotFound)
		}

		return models.Session{}, fmt.Errorf("%s: %w", op, err)
	}

	return session, nil
}

func (s *Storage) GetSessionsByPatient(ctx context.Context, id int64) ([]models.Session, error) {
	const op = "storage.postgres.GetSessionsByPatient"

	stmt, err := s.db.Prepare("SELECT id, patient_id, doctor_id, created_at FROM sessions WHERE patient_id=$1")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrSessionNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var sessions []models.Session
	for rows.Next() {
		var session models.Session
		err = rows.Scan(&session.ID, &session.PatientID, &session.DoctorID, &session.CreatedAt)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		sessions = append(sessions, session)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return sessions, nil
}

func (s *Storage) GetSessionsByDoctor(ctx context.Context, id int64) ([]models.Session, error) {
	const op = "storage.postgres.GetSessionsByDoctor"

	stmt, err := s.db.Prepare("SELECT id, patient_id, doctor_id, created_at FROM sessions WHERE doctor_id=$1")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrSessionNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var sessions []models.Session
	for rows.Next() {
		var session models.Session
		err = rows.Scan(&session.ID, &session.PatientID, &session.DoctorID, &session.CreatedAt)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		sessions = append(sessions, session)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return sessions, nil
}
