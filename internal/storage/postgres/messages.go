package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
)

func (s *Storage) SaveMessage(ctx context.Context, message models.Message) (int64, error) {
	const op = "storage.postgres.SaveMessage"

	stmt, err := s.db.Prepare("INSERT INTO messages (session_id, sender_id, message, send_at) VALUES ($1, $2, $3, $4) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, message.SessionID, message.SenderID, message.Message, message.SendAt)

	var messageId int64
	err = row.Scan(&messageId)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrMessageExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return messageId, nil
}

func (s *Storage) GetMessagesBySession(ctx context.Context, sessionId int64) ([]models.Message, error) {
	const op = "storage.postgres.GetMessagesBySession"

	stmt, err := s.db.Prepare("SELECT id, session_id, sender_id, message, send_at FROM messages WHERE session_id=$1")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, sessionId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrMessageNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var messages []models.Message
	for rows.Next() {
		var message models.Message
		err = rows.Scan(&message.ID, &message.SessionID, &message.SenderID, &message.Message, &message.SendAt)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		messages = append(messages, message)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return messages, nil
}
