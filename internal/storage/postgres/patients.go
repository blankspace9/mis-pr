package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
)

func (s *Storage) GetPatientByEmail(ctx context.Context, email string) (models.User, error) {
	const op = "storage.postgres.GetPatientByEmail"

	stmt, err := s.db.Prepare("SELECT id, email, pass_hash, first_name, last_name, patronymic, phone_number, role, created_at FROM users WHERE email=$1")
	if err != nil {
		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, email)

	var patient models.User
	err = row.Scan(&patient.ID, &patient.Email, &patient.Password, &patient.Firstname, &patient.Lastname, &patient.Patronymic, &patient.PhoneNumber, &patient.Role, &patient.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.User{}, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	return patient, nil
}

func (s *Storage) GetPatientById(ctx context.Context, id int64) (models.User, error) {
	const op = "storage.postgres.GetPatientById"

	stmt, err := s.db.Prepare("SELECT id, email, pass_hash, first_name, last_name, patronymic, phone_number, role, created_at FROM users WHERE id=$1")
	if err != nil {
		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, id)

	var patient models.User
	err = row.Scan(&patient.ID, &patient.Email, &patient.Password, &patient.Firstname, &patient.Lastname, &patient.Patronymic, &patient.PhoneNumber, &patient.Role, &patient.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.User{}, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	return patient, nil
}

func (s *Storage) GetPatientPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error) {
	const op = "storage.postgres.GetPatientPrescriptions"

	stmt, err := s.db.Prepare("SELECT id, patient_id, doctor_id, date_of_issue, duration_days, title, dosage_mg, way_to_use, created_at FROM prescriptions WHERE patient_id=$1")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var prescriptions []models.Prescription
	for rows.Next() {
		var prescription models.Prescription
		err = rows.Scan(&prescription.ID, &prescription.PatientID, &prescription.DoctorID, &prescription.DateOfIssue, &prescription.DurationDays, &prescription.Title, &prescription.DosageMg, &prescription.WayToUse, &prescription.CreatedAt)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		prescriptions = append(prescriptions, prescription)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return prescriptions, nil
}

func (s *Storage) GetPatientDoctors(ctx context.Context, id int64) ([]models.Doctor, error) {
	const op = "storage.postgres.GetPatientDoctors"

	stmt, err := s.db.Prepare(
		`SELECT DISTINCT p.doctor_id, u.email, u.pass_hash, u.first_name, u.last_name, u.patronymic, u.phone_number, u.created_at, d.specialty, d. license FROM prescriptions p
		JOIN users u ON p.doctor_id = u.id
		JOIN doctors d ON u.id = d.user_id
		WHERE p.patient_id=$1`)
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, id)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var doctors []models.Doctor
	for rows.Next() {
		var doctor models.Doctor
		err = rows.Scan(&doctor.ID, &doctor.Email, &doctor.Password, &doctor.Firstname, &doctor.Lastname, &doctor.Patronymic, &doctor.PhoneNumber, &doctor.CreatedAt, &doctor.Specialty, &doctor.License)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		doctors = append(doctors, doctor)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return doctors, nil
}

func (s *Storage) DeletePatient(ctx context.Context, id int64) error {
	const op = "storage.postgres.DeletePatient"

	stmt, err := s.db.Prepare("DELETE FROM users WHERE id=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}
