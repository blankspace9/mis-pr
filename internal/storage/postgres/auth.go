package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"time"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
)

func (s *Storage) SavePatient(ctx context.Context, patientInfo models.User) (int64, error) {
	const op = "storage.postgres.SavePatient"

	stmt, err := s.db.Prepare("INSERT INTO users(email, pass_hash, first_name, last_name, patronymic, phone_number, role, created_at) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, patientInfo.Email, patientInfo.Password, patientInfo.Firstname, patientInfo.Lastname, patientInfo.Patronymic, patientInfo.PhoneNumber, "Patient", time.Now())

	var uid int64
	err = row.Scan(&uid)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrUserExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return uid, nil
}

func (s *Storage) SaveDoctor(ctx context.Context, doctorInfo models.Doctor) (int64, error) {
	const op = "storage.postgres.SavePatient"

	tx, err := s.db.Begin()
	if err != nil {
		return -1, err
	}

	stmtUser, err := s.db.Prepare("INSERT INTO users(email, pass_hash, first_name, last_name, patronymic, phone_number, role, created_at) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	stmtDoctor, err := s.db.Prepare("INSERT INTO doctors(user_id, specialty, license) VALUES($1, $2, $3)")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmtUser.QueryRowContext(ctx, doctorInfo.Email, doctorInfo.Password, doctorInfo.Firstname, doctorInfo.Lastname, doctorInfo.Patronymic, doctorInfo.PhoneNumber, "Doctor", time.Now())

	var uid int64
	err = row.Scan(&uid)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrUserExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	_, err = stmtDoctor.ExecContext(ctx, uid, doctorInfo.Specialty, doctorInfo.License)
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	err = tx.Commit()
	if err != nil {
		return -1, err
	}

	return uid, nil
}

func (s *Storage) SaveAdmin(ctx context.Context, adminInfo models.User) (int64, error) {
	const op = "storage.postgres.SaveAdmin"

	stmt, err := s.db.Prepare("INSERT INTO users(email, pass_hash, first_name, last_name, patronymic, phone_number, role, created_at) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, adminInfo.Email, adminInfo.Password, "Admin", "Adminov", "Adminovich", "78005553535", "Admin", time.Now())

	var uid int64
	err = row.Scan(&uid)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrUserExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return uid, nil
}

func (s *Storage) GetUser(ctx context.Context, email string) (models.User, error) {
	const op = "storage.postgres.GetUser"

	stmt, err := s.db.Prepare("SELECT id, email, pass_hash, first_name, last_name, patronymic, phone_number, role, created_at FROM users WHERE email=$1")
	if err != nil {
		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, email)

	var user models.User
	err = row.Scan(&user.ID, &user.Email, &user.Password, &user.Firstname, &user.Lastname, &user.Patronymic, &user.PhoneNumber, &user.Role, &user.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.User{}, fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
		}

		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	return user, nil
}
