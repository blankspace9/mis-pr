package postgres

import (
	"context"
	"database/sql"
	"errors"
	"fmt"
	"strings"
	"time"

	"github.com/lib/pq"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
)

func (s *Storage) SavePrescription(ctx context.Context, prescription models.Prescription) (int64, error) {
	const op = "storage.postgres.SavePrescription"

	stmt, err := s.db.Prepare("INSERT INTO prescriptions(patient_id, doctor_id, date_of_issue, duration_days, title, dosage_mg, way_to_use, created_at) VALUES($1, $2, $3, $4, $5, $6, $7, $8) RETURNING id")
	if err != nil {
		return 0, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, prescription.PatientID, prescription.DoctorID, prescription.DateOfIssue, prescription.DurationDays, prescription.Title, prescription.DosageMg, prescription.WayToUse, time.Now())

	var uid int64
	err = row.Scan(&uid)
	if err != nil {
		var pgErr *pq.Error
		if errors.As(err, &pgErr) && pgErr.Code == "23505" {
			return 0, fmt.Errorf("%s: %w", op, storage.ErrPrescriptionExists)
		}

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return uid, nil
}

func (s *Storage) GetPrescriptionById(ctx context.Context, id int64) (models.Prescription, error) {
	const op = "storage.postgres.GetPrescriptionById"

	stmt, err := s.db.Prepare("SELECT id, patient_id, doctor_id, date_of_issue, duration_days, title, dosage_mg, way_to_use, created_at FROM prescriptions WHERE id=$1")
	if err != nil {
		return models.Prescription{}, fmt.Errorf("%s: %w", op, err)
	}

	row := stmt.QueryRowContext(ctx, id)

	var prescription models.Prescription
	err = row.Scan(&prescription.ID, &prescription.PatientID, &prescription.DoctorID, &prescription.DateOfIssue, &prescription.DurationDays, &prescription.Title, &prescription.DosageMg, &prescription.WayToUse, &prescription.CreatedAt)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return models.Prescription{}, fmt.Errorf("%s: %w", op, storage.ErrPrescriptionNotFound)
		}

		return models.Prescription{}, fmt.Errorf("%s: %w", op, err)
	}

	return prescription, nil
}

func (s *Storage) GetPrescriptionsByBoth(ctx context.Context, patientId, doctorId int64) ([]models.Prescription, error) {
	const op = "storage.postgres.GetPrescriptionByBoth"

	stmt, err := s.db.Prepare("SELECT id, patient_id, doctor_id, date_of_issue, duration_days, title, dosage_mg, way_to_use, created_at FROM prescriptions WHERE patient_id=$1 AND doctor_id=$2")
	if err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	rows, err := stmt.QueryContext(ctx, patientId, doctorId)
	if err != nil {
		if errors.Is(err, sql.ErrNoRows) {
			return nil, fmt.Errorf("%s: %w", op, storage.ErrPrescriptionNotFound)
		}

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	var prescriptions []models.Prescription
	for rows.Next() {
		var prescription models.Prescription
		err = rows.Scan(&prescription.ID, &prescription.PatientID, &prescription.DoctorID, &prescription.DateOfIssue, &prescription.DurationDays, &prescription.Title, &prescription.DosageMg, &prescription.WayToUse, &prescription.CreatedAt)
		if err != nil {
			return nil, fmt.Errorf("%s: %w", op, err)
		}
		prescriptions = append(prescriptions, prescription)
	}

	if err = rows.Close(); err != nil {
		return nil, fmt.Errorf("%s: %w", op, err)
	}

	return prescriptions, nil
}

func (s *Storage) DeletePrescription(ctx context.Context, id int64) error {
	const op = "storage.postgres.DeletePrescription"

	stmt, err := s.db.Prepare("DELETE FROM prescriptions WHERE id=$1")
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	result, err := stmt.ExecContext(ctx, id)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := result.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrUserNotFound)
	}

	return nil
}

func (s *Storage) UpdatePrescription(ctx context.Context, id int64, prescription models.PrescriptionUpdate) error {
	const op = "storage.postgres.UpdatePrescription"

	setValues := make([]string, 0)
	args := make([]interface{}, 0)
	argId := 1

	if prescription.PatientID != nil {
		setValues = append(setValues, fmt.Sprintf("patient_id=$%d", argId))
		args = append(args, *prescription.PatientID)
		argId++
	}

	if prescription.DoctorID != nil {
		setValues = append(setValues, fmt.Sprintf("doctor_id=$%d", argId))
		args = append(args, *prescription.DoctorID)
		argId++
	}

	if prescription.DateOfIssue != nil {
		setValues = append(setValues, fmt.Sprintf("date_of_issue=$%d", argId))
		args = append(args, *prescription.DateOfIssue)
		argId++
	}

	if prescription.DurationDays != nil {
		setValues = append(setValues, fmt.Sprintf("duration_days=$%d", argId))
		args = append(args, *prescription.DurationDays)
		argId++
	}

	if prescription.Title != nil {
		setValues = append(setValues, fmt.Sprintf("title=$%d", argId))
		args = append(args, *prescription.Title)
		argId++
	}

	if prescription.DosageMg != nil {
		setValues = append(setValues, fmt.Sprintf("dosage_mg=$%d", argId))
		args = append(args, *prescription.DosageMg)
		argId++
	}

	if prescription.WayToUse != nil {
		setValues = append(setValues, fmt.Sprintf("way_to_use=$%d", argId))
		args = append(args, *prescription.WayToUse)
		argId++
	}

	if prescription.CreatedAt != nil {
		setValues = append(setValues, fmt.Sprintf("created_at=$%d", argId))
		args = append(args, pq.FormatTimestamp(*prescription.CreatedAt))
		argId++
	}

	setQuery := strings.Join(setValues, ", ")

	query := fmt.Sprintf("UPDATE prescriptions SET %s WHERE id=$%d", setQuery, argId)
	args = append(args, id)

	// fmt.Println("QUERY:", query, "\nARGS:", args)

	res, err := s.db.Exec(query, args...)
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	rowsAffected, err := res.RowsAffected()
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	if rowsAffected == 0 {
		return fmt.Errorf("%s: %w", op, storage.ErrPrescriptionNotFound)
	}

	return nil
}
