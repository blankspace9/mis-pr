package storage

import "errors"

var (
	ErrUserExists         = errors.New("user already exists")
	ErrPrescriptionExists = errors.New("prescription already exists")
	ErrSessionExists      = errors.New("session already exists")
	ErrMessageExists      = errors.New("message already exists")

	ErrUserNotFound         = errors.New("user not found")
	ErrPrescriptionNotFound = errors.New("prescription not found")
	ErrSessionNotFound      = errors.New("session not found")
	ErrMessageNotFound      = errors.New("message not found")
)
