package mis

import (
	"context"

	valid "github.com/asaskevich/govalidator"
	misv1 "gitlab.com/blankspace9/protos/gen/go/mis"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *ServerAPI) GetPatientByEmail(ctx context.Context, req *misv1.EmailRequest) (*misv1.PatientResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return nil, status.Error(codes.InvalidArgument, "invalid email")
	}

	patient, err := s.patients.GetPatientByEmail(ctx, req.GetEmail())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.PatientResponse{
		Id:          patient.ID,
		Email:       patient.Email,
		Password:    string(patient.Password),
		FirstName:   patient.Firstname,
		LastName:    patient.Lastname,
		Patronymic:  patient.Patronymic,
		PhoneNumber: patient.PhoneNumber,
	}, nil
}

func (s *ServerAPI) GetPatientByID(ctx context.Context, req *misv1.IDRequest) (*misv1.PatientResponse, error) {
	if req.GetId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	patient, err := s.patients.GetPatientByID(ctx, req.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.PatientResponse{
		Id:          patient.ID,
		Email:       patient.Email,
		Password:    string(patient.Password),
		FirstName:   patient.Firstname,
		LastName:    patient.Lastname,
		Patronymic:  patient.Patronymic,
		PhoneNumber: patient.PhoneNumber,
	}, nil
}

func (s *ServerAPI) GetPatientPrescriptions(ctx context.Context, req *misv1.IDRequest) (*misv1.PrescriptionList, error) {
	if req.GetId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	prescriptions, err := s.patients.GetPatientPrescriptions(ctx, req.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var response []*misv1.PrescriptionResponse
	for _, pr := range prescriptions {
		response = append(response, &misv1.PrescriptionResponse{
			Id:           pr.ID,
			PatientId:    pr.PatientID,
			DoctorId:     pr.DoctorID,
			DateOfIssue:  pr.DateOfIssue.Format(dateLayout),
			DurationDays: pr.DurationDays,
			Title:        pr.Title,
			DosageMg:     pr.DosageMg,
			WayToUse:     pr.WayToUse,
		})
	}

	return &misv1.PrescriptionList{
		Prescriptions: response,
	}, nil
}

func (s *ServerAPI) GetPatientDoctors(ctx context.Context, req *misv1.IDRequest) (*misv1.DoctorList, error) {
	if req.GetId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	doctors, err := s.patients.GetPatientDoctors(ctx, req.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var response []*misv1.DoctorResponse
	for _, doctor := range doctors {
		response = append(response, &misv1.DoctorResponse{
			Id:          doctor.ID,
			Email:       doctor.Email,
			Password:    string(doctor.Password),
			FirstName:   doctor.Firstname,
			LastName:    doctor.Lastname,
			Patronymic:  doctor.Patronymic,
			PhoneNumber: doctor.PhoneNumber,
			Specialty:   doctor.Specialty,
			License:     doctor.License,
		})
	}

	return &misv1.DoctorList{
		Doctors: response,
	}, nil
}

func (s *ServerAPI) DeletePatient(ctx context.Context, req *misv1.IDRequest) (*misv1.SuccessResponse, error) {
	if req.GetId() <= emptyValue {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.InvalidArgument, "invalid id")
	}

	err := s.patients.DeletePatient(ctx, req.GetId())
	if err != nil {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.SuccessResponse{
		Success: true,
	}, nil
}
