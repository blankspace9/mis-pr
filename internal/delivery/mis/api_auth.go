package mis

import (
	"context"
	"errors"

	valid "github.com/asaskevich/govalidator"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/lib/validation"
	"gitlab.com/blankspace9/mis-pr/internal/services"
	misv1 "gitlab.com/blankspace9/protos/gen/go/mis"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *ServerAPI) RegisterPatient(ctx context.Context, req *misv1.PatientRequest) (*misv1.CreateResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid email")
	}

	if !validation.ValidatePassword(req.GetPassword()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid password")
	}

	if req.GetFirstName() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "first name required")
	}

	if req.GetLastName() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "last name required")
	}

	if req.GetPatronymic() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "patronymic name required")
	}

	if !valid.IsE164(req.GetPhoneNumber()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid phone number")
	}

	id, err := s.authLogin.RegisterPatient(ctx, models.User{
		Email:       req.GetEmail(),
		Password:    []byte(req.GetPassword()),
		Firstname:   req.GetFirstName(),
		Lastname:    req.GetLastName(),
		Patronymic:  req.GetPatronymic(),
		PhoneNumber: req.GetPhoneNumber(),
	})
	if err != nil {
		if errors.Is(err, services.ErrUserAlreadyExists) {
			return &misv1.CreateResponse{
				Success: false,
				Id:      0,
			}, status.Error(codes.AlreadyExists, "user already exists")
		}

		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.Internal, "internal error")
	}

	return &misv1.CreateResponse{
		Success: true,
		Id:      id,
	}, nil
}

func (s *ServerAPI) RegisterDoctor(ctx context.Context, req *misv1.DoctorRequest) (*misv1.CreateResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid email")
	}

	if !validation.ValidatePassword(req.GetPassword()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid password")
	}

	if req.GetFirstName() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "first name required")
	}

	if req.GetLastName() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "last name required")
	}

	if req.GetPatronymic() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "patronymic name required")
	}

	if !valid.IsE164(req.GetPhoneNumber()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid phone number")
	}

	if req.GetSpecialty() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "specialty required")
	}

	if req.GetLicense() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "license required")
	}

	id, err := s.authLogin.RegisterDoctor(ctx, models.Doctor{
		Email:       req.GetEmail(),
		Password:    []byte(req.GetPassword()),
		Firstname:   req.GetFirstName(),
		Lastname:    req.GetLastName(),
		Patronymic:  req.GetPatronymic(),
		PhoneNumber: req.GetPhoneNumber(),
		Specialty:   req.GetSpecialty(),
		License:     req.GetLicense(),
	})
	if err != nil {
		if errors.Is(err, services.ErrUserAlreadyExists) {
			return &misv1.CreateResponse{
				Success: false,
				Id:      0,
			}, status.Error(codes.AlreadyExists, "user already exists")
		}

		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.Internal, "internal error")
	}

	return &misv1.CreateResponse{
		Success: true,
		Id:      id,
	}, nil
}

func (s *ServerAPI) RegisterAdmin(ctx context.Context, req *misv1.AdminRequest) (*misv1.CreateResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid email")
	}

	if !validation.ValidatePassword(req.GetPassword()) {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid password")
	}

	id, err := s.authLogin.RegisterAdmin(ctx, models.User{
		Email:    req.GetEmail(),
		Password: []byte(req.GetPassword()),
	})
	if err != nil {
		if errors.Is(err, services.ErrUserAlreadyExists) {
			return &misv1.CreateResponse{
				Success: false,
				Id:      0,
			}, status.Error(codes.AlreadyExists, "admin already exists")
		}

		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.Internal, "internal error")
	}

	return &misv1.CreateResponse{
		Success: true,
		Id:      id,
	}, nil
}

func (s *ServerAPI) Login(ctx context.Context, req *misv1.LoginRequest) (*misv1.LoginResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return nil, status.Error(codes.InvalidArgument, "invalid email")
	}

	if !validation.ValidatePassword(req.GetPassword()) {
		return nil, status.Error(codes.InvalidArgument, "password required")
	}

	id, accessToken, _, err := s.authLogin.Login(ctx, req.GetEmail(), req.GetPassword())
	if err != nil {
		if errors.Is(err, services.ErrInvalidCredentials) {
			return nil, status.Error(codes.InvalidArgument, "invalid credentials")
		}

		return nil, status.Error(codes.Internal, "internal error")
	}

	return &misv1.LoginResponse{
		Token: accessToken,
		Id:    id,
	}, nil
}
