package mis

import (
	"context"
	"time"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	misv1 "gitlab.com/blankspace9/protos/gen/go/mis"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *ServerAPI) AddPrescription(ctx context.Context, req *misv1.PrescriptionRequest) (*misv1.CreateResponse, error) {
	if req.GetPatientId() <= emptyValue {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid patient id")
	}

	if req.GetDoctorId() <= emptyValue {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid doctor id")
	}

	if req.GetDurationDays() <= emptyValue {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid duration days")
	}

	if req.GetTitle() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "title required")
	}

	if req.GetDosageMg() <= emptyValue {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid dosage")
	}

	if req.GetWayToUse() == "" {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "way to use required")
	}

	t, err := time.Parse(dateLayout, req.GetDateOfIssue())
	if err != nil {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid date of issue format")
	}
	if t.Unix() < time.Now().Unix() {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.InvalidArgument, "invalid date of issue")
	}

	id, err := s.prescriptions.AddPrescription(ctx, models.Prescription{
		PatientID:    req.GetPatientId(),
		DoctorID:     req.GetDoctorId(),
		DateOfIssue:  t,
		DurationDays: req.GetDurationDays(),
		Title:        req.GetTitle(),
		DosageMg:     req.GetDosageMg(),
		WayToUse:     req.GetWayToUse(),
	})
	if err != nil {
		return &misv1.CreateResponse{
			Success: false,
			Id:      0,
		}, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.CreateResponse{
		Success: true,
		Id:      id,
	}, nil
}

func (s *ServerAPI) GetPrescriptionByID(ctx context.Context, req *misv1.IDRequest) (*misv1.PrescriptionResponse, error) {
	if req.GetId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	prescription, err := s.prescriptions.GetPrescriptionByID(ctx, req.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.PrescriptionResponse{
		Id:           prescription.ID,
		PatientId:    prescription.PatientID,
		DoctorId:     prescription.DoctorID,
		DateOfIssue:  prescription.DateOfIssue.Format(dateLayout),
		DurationDays: prescription.DurationDays,
		Title:        prescription.Title,
		DosageMg:     prescription.DosageMg,
		WayToUse:     prescription.WayToUse,
	}, nil
}

func (s *ServerAPI) GetPrescriptionsByBoth(ctx context.Context, req *misv1.PatientDoctorIDRequest) (*misv1.PrescriptionList, error) {
	if req.GetPatientId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid patient id")
	}

	if req.GetDoctorId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid doctor id")
	}

	prescriptions, err := s.prescriptions.GetPrescriptionsByBoth(ctx, req.GetPatientId(), req.GetDoctorId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var response []*misv1.PrescriptionResponse
	for _, pr := range prescriptions {
		response = append(response, &misv1.PrescriptionResponse{
			Id:           pr.ID,
			PatientId:    pr.PatientID,
			DoctorId:     pr.DoctorID,
			DateOfIssue:  pr.DateOfIssue.Format(dateLayout),
			DurationDays: pr.DurationDays,
			Title:        pr.Title,
			DosageMg:     pr.DosageMg,
			WayToUse:     pr.WayToUse,
		})
	}

	return &misv1.PrescriptionList{
		Prescriptions: response,
	}, nil
}

func (s *ServerAPI) DeletePrescription(ctx context.Context, req *misv1.IDRequest) (*misv1.SuccessResponse, error) {
	if req.GetId() <= emptyValue {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.InvalidArgument, "invalid id")
	}

	err := s.prescriptions.DeletePrescription(ctx, req.GetId())
	if err != nil {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.SuccessResponse{
		Success: true,
	}, nil
}

func (s *ServerAPI) UpdatePrescription(ctx context.Context, req *misv1.PrescriptionUpdateRequest) (*misv1.SuccessResponse, error) {
	if req.GetId() <= emptyValue {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.InvalidArgument, "invalid id")
	}

	var t *time.Time
	var t1 time.Time
	var err error
	if req.DateOfIssue != nil {
		t1, err = time.Parse(dateLayout, req.GetDateOfIssue())
		if err != nil {
			return &misv1.SuccessResponse{
				Success: false,
			}, status.Error(codes.InvalidArgument, "invalid date of issue format")
		}
		t = &t1
		if t.Unix() < time.Now().Unix() {
			return &misv1.SuccessResponse{
				Success: false,
			}, status.Error(codes.InvalidArgument, "invalid date of issue")
		}
	}

	err = s.prescriptions.UpdatePrescription(ctx, req.GetId(), models.PrescriptionUpdate{
		PatientID:    req.PatientId,
		DoctorID:     req.DoctorId,
		DateOfIssue:  t,
		DurationDays: req.DurationDays,
		Title:        req.Title,
		DosageMg:     req.DosageMg,
		WayToUse:     req.WayToUse,
	})
	if err != nil {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.SuccessResponse{
		Success: true,
	}, nil
}
