package mis

import (
	"context"
	"time"

	"gitlab.com/blankspace9/mis-pr/internal/domain/middleware"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	misv1 "gitlab.com/blankspace9/protos/gen/go/mis"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"google.golang.org/protobuf/types/known/timestamppb"
)

func (s *ServerAPI) SendMessage(ctx context.Context, req *misv1.Message) (*misv1.SuccessResponse, error) {
	if req.GetSenderId() <= emptyValue {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.InvalidArgument, "invalid sender id")
	}

	if req.GetReceiverId() <= emptyValue {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.InvalidArgument, "invalid receiver id")
	}

	if req.GetMessage() == "" {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.InvalidArgument, "empty message")
	}

	var who int64
	if ctx.Value(middleware.CtxUserRole).(string) == "Patient" {
		who = 1
	} else {
		who = 0
	}

	sessionId, err := s.chat.GetSessionIdByMembers(ctx, req.GetSenderId(), req.GetReceiverId())
	if err != nil {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.Internal, "internal server error")
	}

	streams, ok := s.clients[sessionId]
	if ok {
		stream := streams[who]
		if stream != nil {
			err = (*stream).Send(&misv1.Message{
				Message:    req.GetMessage(),
				SenderId:   req.GetSenderId(),
				ReceiverId: req.GetReceiverId(),
				CreatedAt:  timestamppb.Now(),
			})
			if err != nil {
				return nil, status.Error(codes.Internal, "internal server error")
			}
		}
	}
	s.chat.Save(ctx, models.Message{
		Message:    req.GetMessage(),
		SessionID:  sessionId,
		SenderID:   req.GetSenderId(),
		ReceiverID: req.GetReceiverId(),
		SendAt:     time.Now(),
	})

	return &misv1.SuccessResponse{
		Success: true,
	}, nil
}

func (s *ServerAPI) ReceiveMessages(req *misv1.JoinSessionRequest, stream misv1.ChatService_ReceiveMessagesServer) error {
	if req.GetReceiverId() <= emptyValue {
		return status.Error(codes.InvalidArgument, "invalid receiver id")
	}

	if req.GetSenderId() <= emptyValue {
		return status.Error(codes.InvalidArgument, "invalid session id")
	}

	var who int64
	if stream.Context().Value(middleware.CtxUserRole).(string) == "Patient" {
		who = 0
	} else {
		who = 1
	}

	sessionId, err := s.chat.Join(stream.Context(), req.GetSenderId(), req.GetReceiverId())
	if err != nil {
		return status.Error(codes.Internal, "internal server error")
	}

	s.mu.Lock()
	temp := s.clients[sessionId]
	strm := &temp[who]
	*strm = &stream
	s.clients[sessionId] = temp
	s.mu.Unlock()

	defer func() {
		s.mu.Lock()
		delete(s.clients, req.ReceiverId)
		s.mu.Unlock()
	}() // Удаляем поток из глобальной переменной при закрытии соединения.

	for {
		// Проверяем, не произошло ли закрытие потока (соединения).
		select {
		case <-s.closeCh:
			return status.Error(codes.Canceled, "server closed connection")
		default:
			if stream.Context().Err() != nil {
				return nil
			}
		}
	}
}
