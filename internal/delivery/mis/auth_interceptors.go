package mis

import (
	"context"
	"strings"

	"gitlab.com/blankspace9/mis-pr/internal/domain/middleware"
	"google.golang.org/grpc"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/metadata"
	"google.golang.org/grpc/status"
)

func (s *ServerAPI) JWTAuthInterceptor() grpc.UnaryServerInterceptor {
	return func(
		ctx context.Context,
		req interface{},
		info *grpc.UnaryServerInfo,
		handler grpc.UnaryHandler,
	) (interface{}, error) {
		allowedRoles, ok := middleware.AccessControlList.MethodRoles[info.FullMethod]
		if !ok {
			return nil, status.Error(codes.Unavailable, "access control rules not defined for method")
		}

		if len(allowedRoles) == 0 {
			return handler(ctx, req)
		}

		md, ok := metadata.FromIncomingContext(ctx)
		if !ok {
			return nil, status.Error(codes.Unauthenticated, "missing metadata")
		}

		authorization := md["authorization"]
		if len(authorization) == 0 {
			return nil, status.Error(codes.Unauthenticated, "missing authorization header")
		}

		tokenString := strings.TrimPrefix(authorization[0], "Bearer ")

		id, role, err := s.authLogin.ParseToken(context.TODO(), tokenString)
		if err != nil {
			return nil, err
		}

		if !isRoleAllowed(role, allowedRoles) {
			return nil, status.Error(codes.PermissionDenied, "access denied")
		}

		ctx = context.WithValue(ctx, middleware.CtxUserID, id)
		ctx = context.WithValue(ctx, middleware.CtxUserRole, role)
		return handler(ctx, req)
	}
}

func (s *ServerAPI) StreamServerInterceptor(srv interface{}, stream grpc.ServerStream, info *grpc.StreamServerInfo, handler grpc.StreamHandler) error {
	allowedRoles, ok := middleware.AccessControlList.MethodRoles[info.FullMethod]
	if !ok {
		return status.Error(codes.Unavailable, "access control rules not defined for method")
	}

	if len(allowedRoles) == 0 {
		return handler(srv, stream)
	}

	md, ok := metadata.FromIncomingContext(stream.Context())
	if !ok {
		return status.Error(codes.Unauthenticated, "missing metadata")
	}

	authorization := md["authorization"]
	if len(authorization) == 0 {
		return status.Error(codes.Unauthenticated, "missing authorization header")
	}

	tokenString := strings.TrimPrefix(authorization[0], "Bearer ")

	id, role, err := s.authLogin.ParseToken(context.TODO(), tokenString)
	if err != nil {
		return err
	}

	if !isRoleAllowed(role, allowedRoles) {
		return status.Error(codes.PermissionDenied, "access denied")
	}

	// Создаем новый контекст с добавленными значениями
	ctx := context.WithValue(stream.Context(), middleware.CtxUserID, id)
	ctx = context.WithValue(ctx, middleware.CtxUserRole, role)

	if sessionID, ok := md["session_id"]; ok {
		ctx = context.WithValue(ctx, middleware.CtxSessionID, sessionID[0])
	}

	err = handler(srv, &serverStreamWithContext{ServerStream: stream, ctx: ctx})

	return err
}

// func (s *ServerAPI) StreamServerInterceptor() grpc.StreamServerInterceptor {
// 	return func(
// 		srv any,
// 		stream grpc.ServerStream,
// 		info *grpc.StreamServerInfo,
// 		handler grpc.StreamHandler,
// 	) error {
// 		allowedRoles, ok := middleware.AccessControlList.MethodRoles[info.FullMethod]
// 		if !ok {
// 			return status.Error(codes.Unavailable, "access control rules not defined for method")
// 		}

// 		if len(allowedRoles) == 0 {
// 			return handler(srv, stream)
// 		}

// 		md, ok := metadata.FromIncomingContext(stream.Context())
// 		if !ok {
// 			return status.Error(codes.Unauthenticated, "missing metadata")
// 		}

// 		authorization := md["authorization"]
// 		if len(authorization) == 0 {
// 			return status.Error(codes.Unauthenticated, "missing authorization header")
// 		}

// 		tokenString := strings.TrimPrefix(authorization[0], "Bearer ")

// 		id, role, err := s.authLogin.ParseToken(context.TODO(), tokenString)
// 		if err != nil {
// 			return err
// 		}

// 		if !isRoleAllowed(role, allowedRoles) {
// 			return status.Error(codes.PermissionDenied, "access denied")
// 		}

// 		// Создаем новый контекст с добавленными значениями
// 		ctx := context.WithValue(stream.Context(), middleware.CtxUserID, id)
// 		ctx = context.WithValue(ctx, middleware.CtxUserRole, role)

// 		if sessionID, ok := md["session_id"]; ok {
// 			ctx = context.WithValue(ctx, middleware.CtxSessionID, sessionID[0])
// 		}

// 		fmt.Println(ctx)

// 		err = handler(srv, &serverStreamWithContext{ServerStream: stream, ctx: ctx})

// 		return err
// 	}
// }

// serverStreamWithContext обертка для grpc.ServerStream с новым контекстом
type serverStreamWithContext struct {
	grpc.ServerStream
	ctx context.Context
}

func (css *serverStreamWithContext) Context() context.Context {
	return css.ctx
}

// Функция для проверки, разрешена ли роль для метода
func isRoleAllowed(role string, allowedRoles []string) bool {
	if len(allowedRoles) == 0 {
		return true // Если список ролей пуст, доступ разрешен для всех
	}
	for _, allowedRole := range allowedRoles {
		if role == allowedRole {
			return true
		}
	}
	return false
}
