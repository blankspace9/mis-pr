package mis

import (
	"context"
	"sync"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	misv1 "gitlab.com/blankspace9/protos/gen/go/mis"
	"google.golang.org/grpc"
)

const (
	emptyValue = 0
	dateLayout = "2006.01.02"
)

type AuthLogin interface {
	RegisterPatient(ctx context.Context, patientInfo models.User) (int64, error)
	RegisterDoctor(ctx context.Context, doctorInfo models.Doctor) (int64, error)
	RegisterAdmin(ctx context.Context, adminInfo models.User) (int64, error)
	Login(ctx context.Context, email, password string) (int64, string, string, error)
	ParseToken(ctx context.Context, token string) (int64, string, error)
}

type PatientsManager interface {
	GetPatientByEmail(ctx context.Context, email string) (models.User, error)
	GetPatientByID(ctx context.Context, id int64) (models.User, error)
	GetPatientPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error)
	GetPatientDoctors(ctx context.Context, id int64) ([]models.Doctor, error)
	DeletePatient(ctx context.Context, id int64) error
}

type DoctorsManager interface {
	GetDoctorByEmail(ctx context.Context, email string) (models.Doctor, error)
	GetDoctorByID(ctx context.Context, id int64) (models.Doctor, error)
	GetDoctorPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error)
	GetDoctorPatients(ctx context.Context, id int64) ([]models.User, error)
	DeleteDoctor(ctx context.Context, id int64) error
}

type PrescriptionsManager interface {
	AddPrescription(ctx context.Context, prescription models.Prescription) (int64, error)
	GetPrescriptionByID(ctx context.Context, id int64) (models.Prescription, error)
	GetPrescriptionsByBoth(ctx context.Context, patientId, doctorId int64) ([]models.Prescription, error)
	UpdatePrescription(ctx context.Context, id int64, prescription models.PrescriptionUpdate) error
	DeletePrescription(ctx context.Context, id int64) error
}

type Chat interface {
	Join(ctx context.Context, senderId, receiverId int64) (int64, error)
	Save(ctx context.Context, message models.Message) (int64, error)
	GetSessionIdByMembers(ctx context.Context, senderId, receiverId int64) (int64, error)
}

type ServerAPI struct {
	misv1.UnimplementedMISServer
	misv1.UnimplementedChatServiceServer
	authLogin     AuthLogin
	patients      PatientsManager
	doctors       DoctorsManager
	prescriptions PrescriptionsManager
	chat          Chat

	mu      sync.Mutex
	clients map[int64][2]*misv1.ChatService_ReceiveMessagesServer
	closeCh chan struct{}
	// chatSessions map[int64]map[int64]misv1.ChatService_ChatStreamServer
}

func RegisterServerAPI(authLogin AuthLogin, patients PatientsManager, doctors DoctorsManager, prescriptions PrescriptionsManager, chat Chat) (*grpc.Server, *ServerAPI) {
	api := &ServerAPI{
		authLogin:     authLogin,
		patients:      patients,
		doctors:       doctors,
		prescriptions: prescriptions,
		chat:          chat,
		clients:       make(map[int64][2]*misv1.ChatService_ReceiveMessagesServer),
		closeCh:       make(chan struct{}, 1),
	}

	gRPCServer := grpc.NewServer(
		grpc.UnaryInterceptor(api.JWTAuthInterceptor()),
		grpc.StreamInterceptor(api.StreamServerInterceptor),
	)

	misv1.RegisterMISServer(gRPCServer, api)
	misv1.RegisterChatServiceServer(gRPCServer, api)

	return gRPCServer, api
}

func (s *ServerAPI) CloseActiveConnections() {
	s.closeCh <- struct{}{}
	close(s.closeCh)
}
