package mis

import (
	"context"

	valid "github.com/asaskevich/govalidator"
	misv1 "gitlab.com/blankspace9/protos/gen/go/mis"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
)

func (s *ServerAPI) GetDoctorByEmail(ctx context.Context, req *misv1.EmailRequest) (*misv1.DoctorResponse, error) {
	if !valid.IsEmail(req.GetEmail()) {
		return nil, status.Error(codes.InvalidArgument, "invalid email")
	}

	doctor, err := s.doctors.GetDoctorByEmail(ctx, req.GetEmail())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.DoctorResponse{
		Id:          doctor.ID,
		Email:       doctor.Email,
		Password:    string(doctor.Password),
		FirstName:   doctor.Firstname,
		LastName:    doctor.Lastname,
		Patronymic:  doctor.Patronymic,
		PhoneNumber: doctor.PhoneNumber,
		Specialty:   doctor.Specialty,
		License:     doctor.License,
	}, nil
}

func (s *ServerAPI) GetDoctorByID(ctx context.Context, req *misv1.IDRequest) (*misv1.DoctorResponse, error) {
	if req.GetId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	doctor, err := s.doctors.GetDoctorByID(ctx, req.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.DoctorResponse{
		Id:          doctor.ID,
		Email:       doctor.Email,
		Password:    string(doctor.Password),
		FirstName:   doctor.Firstname,
		LastName:    doctor.Lastname,
		Patronymic:  doctor.Patronymic,
		PhoneNumber: doctor.PhoneNumber,
		Specialty:   doctor.Specialty,
		License:     doctor.License,
	}, nil
}

func (s *ServerAPI) GetDoctorPrescriptions(ctx context.Context, req *misv1.IDRequest) (*misv1.PrescriptionList, error) {
	if req.GetId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	prescriptions, err := s.doctors.GetDoctorPrescriptions(ctx, req.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var response []*misv1.PrescriptionResponse
	for _, pr := range prescriptions {
		response = append(response, &misv1.PrescriptionResponse{
			Id:           pr.ID,
			PatientId:    pr.PatientID,
			DoctorId:     pr.DoctorID,
			DateOfIssue:  pr.DateOfIssue.Format(dateLayout),
			DurationDays: pr.DurationDays,
			Title:        pr.Title,
			DosageMg:     pr.DosageMg,
			WayToUse:     pr.WayToUse,
		})
	}

	return &misv1.PrescriptionList{
		Prescriptions: response,
	}, nil
}

func (s *ServerAPI) GetDoctorPatients(ctx context.Context, req *misv1.IDRequest) (*misv1.PatientList, error) {
	if req.GetId() <= emptyValue {
		return nil, status.Error(codes.InvalidArgument, "invalid id")
	}

	patients, err := s.doctors.GetDoctorPatients(ctx, req.GetId())
	if err != nil {
		return nil, status.Error(codes.Internal, "internal server error")
	}

	var response []*misv1.PatientResponse
	for _, patient := range patients {
		response = append(response, &misv1.PatientResponse{
			Id:          patient.ID,
			Email:       patient.Email,
			Password:    string(patient.Password),
			FirstName:   patient.Firstname,
			LastName:    patient.Lastname,
			Patronymic:  patient.Patronymic,
			PhoneNumber: patient.PhoneNumber,
		})
	}

	return &misv1.PatientList{
		Patients: response,
	}, nil
}

func (s *ServerAPI) DeleteDoctor(ctx context.Context, req *misv1.IDRequest) (*misv1.SuccessResponse, error) {
	if req.GetId() <= emptyValue {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.InvalidArgument, "invalid id")
	}

	err := s.doctors.DeleteDoctor(ctx, req.GetId())
	if err != nil {
		return &misv1.SuccessResponse{
			Success: false,
		}, status.Error(codes.Internal, "internal server error")
	}

	return &misv1.SuccessResponse{
		Success: true,
	}, nil
}
