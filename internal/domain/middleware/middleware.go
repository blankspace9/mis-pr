package middleware

type CtxKey int

const (
	CtxUserID CtxKey = iota
	CtxUserRole
	CtxSessionID
)

type AccessControl struct {
	MethodRoles map[string][]string
}

// Инициализация правил доступа
var AccessControlList = AccessControl{
	MethodRoles: map[string][]string{
		"/mis.MIS/Login":           {},
		"/mis.MIS/RegisterPatient": {},
		"/mis.MIS/RegisterAdmin":   {},
		"/mis.MIS/RegisterDoctor":  {"Admin"},

		"/mis.MIS/GetPatientByEmail":       {"Patient", "Doctor", "Admin"},
		"/mis.MIS/GetPatientByID":          {"Patient", "Doctor", "Admin"},
		"/mis.MIS/GetPatientPrescriptions": {"Patient", "Doctor", "Admin"},
		"/mis.MIS/GetPatientDoctors":       {"Patient", "Admin"},
		"/mis.MIS/DeletePatient":           {"Patient", "Admin"},

		"/mis.MIS/GetDoctorByEmail":       {"Patient", "Doctor", "Admin"},
		"/mis.MIS/GetDoctorByID":          {"Patient", "Doctor", "Admin"},
		"/mis.MIS/GetDoctorPatients":      {"Doctor", "Admin"},
		"/mis.MIS/GetDoctorPrescriptions": {"Doctor", "Admin"},
		"/mis.MIS/DeleteDoctor":           {"Doctor", "Admin"},

		"/mis.MIS/AddPrescription":        {"Doctor", "Admin"},
		"/mis.MIS/GetPrescriptionByID":    {"Patient", "Doctor", "Admin"},
		"/mis.MIS/GetPrescriptionsByBoth": {"Patient", "Doctor", "Admin"},
		"/mis.MIS/DeletePrescription":     {"Admin"},
		"/mis.MIS/UpdatePrescription":     {"Doctor", "Admin"},

		"/mis.ChatService/ReceiveMessages": {"Patient", "Doctor"},
		"/mis.ChatService/SendMessage":     {"Patient", "Doctor"},
	},
}
