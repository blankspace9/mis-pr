package models

import "time"

type Session struct {
	ID        int64
	PatientID int64
	DoctorID  int64
	CreatedAt time.Time
}
