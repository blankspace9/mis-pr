package models

import "time"

type Message struct {
	ID         int64
	SessionID  int64
	SenderID   int64
	ReceiverID int64
	Message    string
	SendAt     time.Time
}
