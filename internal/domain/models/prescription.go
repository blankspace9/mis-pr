package models

import "time"

type Prescription struct {
	ID           int64
	PatientID    int64
	DoctorID     int64
	DateOfIssue  time.Time
	DurationDays int64
	Title        string
	DosageMg     int64
	WayToUse     string
	CreatedAt    time.Time
}

type PrescriptionUpdate struct {
	ID           int64
	PatientID    *int64
	DoctorID     *int64
	DateOfIssue  *time.Time
	DurationDays *int64
	Title        *string
	DosageMg     *int64
	WayToUse     *string
	CreatedAt    *time.Time
}
