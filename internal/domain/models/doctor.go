package models

import "time"

type Doctor struct {
	ID          int64
	Email       string
	Password    []byte
	Firstname   string
	Lastname    string
	Patronymic  string
	PhoneNumber string
	Specialty   string
	License     string
	CreatedAt   time.Time
}
