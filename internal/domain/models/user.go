package models

import "time"

type User struct {
	ID          int64
	Email       string
	Password    []byte
	Firstname   string
	Lastname    string
	Patronymic  string
	PhoneNumber string
	Role        string
	CreatedAt   time.Time
}
