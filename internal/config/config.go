package config

import (
	"flag"
	"os"
	"time"

	"github.com/ilyakaznacheev/cleanenv"
	"github.com/joho/godotenv"
)

type (
	Config struct {
		Env     string     `yaml:"env" env-default:"local"`
		GRPC    GRPCConfig `yaml:"grpc"`
		Auth    AuthConfig
		Storage PostgresConfig
	}

	MigrateConfig struct {
		DB PostgresConfig
	}

	GRPCConfig struct {
		Port    int           `yaml:"port"`
		Timeout time.Duration `yaml:"timeout"`
	}

	AuthConfig struct {
		SecretKey string       `env:"JWT_SECRET"`
		Tokens    TokensConfig `yaml:"tokens"`
	}

	TokensConfig struct {
		AccessDuration time.Duration `yaml:"access_duration"`
	}

	PostgresConfig struct {
		Host     string `env:"HOST" env-default:"localhost"`
		Port     string `env:"PORT" env-default:"5432"`
		Username string `env:"PG_USER" env-default:"postgres"`
		DBName   string `env:"NAME"`
		SSLMode  string `env:"SSLMODE" env-default:"disable"`
		Password string `env:"PASSWORD"`
	}
)

func MustLoad() *Config {
	path := fetchConfigPath()
	if path == "" {
		panic("config path is empty")
	}

	return MustLoadByPath(path, ".env")
}

func MustLoadByPath(configPath string, envPath string) *Config {
	// check if file exists
	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		panic("config file does not exists: " + configPath)
	}

	var cfg Config

	if err := cleanenv.ReadConfig(configPath, &cfg); err != nil {
		panic("failed to read config: " + err.Error())
	}

	if err := cleanenv.ReadConfig(configPath, &cfg.Auth); err != nil {
		panic("failed to read config: " + err.Error())
	}

	if err := godotenv.Load(envPath); err != nil {
		panic("failed to load .env file: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg); err != nil {
		panic("failed to read config: " + err.Error())
	}

	// if err := cleanenv.ReadEnv(&cfg.SecretKey); err != nil {
	// 	panic("failed to read config: " + err.Error())
	// }

	return &cfg
}

func MigrateMustLoad() *MigrateConfig {
	cfg := new(MigrateConfig)

	if err := godotenv.Load(".env"); err != nil {
		panic("failed to load .env file: " + err.Error())
	}

	if err := cleanenv.ReadEnv(&cfg.DB); err != nil {
		panic("failed to read config: " + err.Error())
	}

	return cfg
}

// flag > env > default
func fetchConfigPath() string {
	var res string

	flag.StringVar(&res, "config", "", "path to config file")
	flag.Parse()

	if res == "" {
		res = os.Getenv("CONFIG_PATH")
	}

	return res
}
