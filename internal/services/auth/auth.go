package auth

import (
	"context"
	"errors"
	"fmt"
	"log/slog"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	jwtlib "gitlab.com/blankspace9/mis-pr/internal/lib/jwt"
	"gitlab.com/blankspace9/mis-pr/internal/lib/logger/sl"
	"gitlab.com/blankspace9/mis-pr/internal/services"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
	"golang.org/x/crypto/bcrypt"
)

type Auth struct {
	userManager AuthManager

	log            *slog.Logger
	secretKey      []byte
	accessDuration time.Duration
}

type AuthManager interface {
	SavePatient(ctx context.Context, patientInfo models.User) (int64, error)
	SaveDoctor(ctx context.Context, doctorInfo models.Doctor) (int64, error)
	SaveAdmin(ctx context.Context, adminInfo models.User) (int64, error)
	GetUser(ctx context.Context, email string) (models.User, error)
}

func New(userManager AuthManager, log *slog.Logger, secretKey string, accessDuration time.Duration) *Auth {
	return &Auth{
		userManager:    userManager,
		log:            log,
		secretKey:      []byte(secretKey),
		accessDuration: accessDuration,
	}
}

func (a *Auth) RegisterPatient(ctx context.Context, patientInfo models.User) (int64, error) {
	const op = "auth.RegisterPatient"

	log := a.log.With(slog.String("op", op))

	log.Info("attempting to register patient")

	passHash, err := bcrypt.GenerateFromPassword([]byte(patientInfo.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Error("failed to generate password hash", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	patientInfo.Password = passHash

	id, err := a.userManager.SavePatient(ctx, patientInfo)
	if err != nil {
		a.log.Error("failed to save patient", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("patient registered")

	return id, nil
}

func (a *Auth) RegisterDoctor(ctx context.Context, doctorInfo models.Doctor) (int64, error) {
	const op = "auth.RegisterDoctor"

	log := a.log.With(slog.String("op", op))

	log.Info("attempting to register doctor")

	passHash, err := bcrypt.GenerateFromPassword([]byte(doctorInfo.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Error("failed to generate password hash", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	doctorInfo.Password = passHash

	id, err := a.userManager.SaveDoctor(ctx, doctorInfo)
	if err != nil {
		a.log.Error("failed to save patient", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("doctor registered")

	return id, nil
}

func (a *Auth) RegisterAdmin(ctx context.Context, adminInfo models.User) (int64, error) {
	const op = "auth.RegisterAdmin"

	log := a.log.With(slog.String("op", op))

	log.Info("attempting to register admin")

	passHash, err := bcrypt.GenerateFromPassword([]byte(adminInfo.Password), bcrypt.DefaultCost)
	if err != nil {
		log.Error("failed to generate password hash", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	adminInfo.Password = passHash

	id, err := a.userManager.SaveAdmin(ctx, adminInfo)
	if err != nil {
		a.log.Error("failed to save admin", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("admin registered")

	return id, nil
}

func (a *Auth) Login(ctx context.Context, email string, password string) (int64, string, string, error) {
	const op = "auth.Login"

	log := a.log.With(slog.String("op", op))

	log.Info("attempting to login user")

	user, err := a.userManager.GetUser(ctx, email)
	if err != nil {
		if errors.Is(err, storage.ErrUserNotFound) {
			a.log.Warn("user not found", sl.Err(err))

			return 0, "", "", fmt.Errorf("%s: %w", op, services.ErrInvalidCredentials)
		}

		a.log.Error("failed to get user", sl.Err(err))

		return 0, "", "", fmt.Errorf("%s: %w", op, err)
	}

	if err := bcrypt.CompareHashAndPassword(user.Password, []byte(password)); err != nil {
		a.log.Warn("invalid credentials", sl.Err(err))

		return 0, "", "", fmt.Errorf("%s: %w", op, services.ErrInvalidCredentials)
	}

	accessToken, refreshToken, err := jwtlib.NewTokens(user.ID, user.Role, a.accessDuration, a.secretKey)
	if err != nil {
		a.log.Error("failed to generate token", sl.Err(err))

		return 0, "", "", fmt.Errorf("%s: %w", op, err)
	}

	return user.ID, accessToken, refreshToken, nil
}

func (s *Auth) ParseToken(ctx context.Context, token string) (int64, string, error) {
	t, err := jwt.ParseWithClaims(token, &jwtlib.Claims{}, func(token *jwt.Token) (interface{}, error) {
		if _, ok := token.Method.(*jwt.SigningMethodHMAC); !ok {
			return nil, fmt.Errorf("unexpected signing method: %v", token.Header["alg"])
		}
		return s.secretKey, nil
	})
	if err != nil {
		return 0, "", err
	}

	if !t.Valid {
		return 0, "", errors.New("invalid token")
	}

	claims, ok := t.Claims.(*jwtlib.Claims)
	if !ok {
		return 0, "", errors.New("invalid claims")
	}

	return claims.Id, claims.Role, nil
}
