package chat

import (
	"context"
	"errors"
	"fmt"
	"log/slog"

	"gitlab.com/blankspace9/mis-pr/internal/domain/middleware"
	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/lib/logger/sl"
	"gitlab.com/blankspace9/mis-pr/internal/storage"
)

type SessionStorage interface {
	CreateSession(ctx context.Context, patientId, doctorId int64) (int64, error)
	GetSessionByMembers(ctx context.Context, patientId, doctorId int64) (models.Session, error)
	GetSessionsByPatient(ctx context.Context, id int64) ([]models.Session, error)
	GetSessionsByDoctor(ctx context.Context, id int64) ([]models.Session, error)
}

type MessageStorage interface {
	SaveMessage(ctx context.Context, message models.Message) (int64, error)
	GetMessagesBySession(ctx context.Context, sessionId int64) ([]models.Message, error)
}

type UserManager interface {
	GetPatientById(ctx context.Context, id int64) (models.User, error)
}

type ChatService struct {
	sessionStorage SessionStorage
	messageStorage MessageStorage
	userManager    UserManager

	log *slog.Logger
}

func New(sessionStorage SessionStorage, messageStorage MessageStorage, userManager UserManager, log *slog.Logger) *ChatService {
	return &ChatService{
		sessionStorage: sessionStorage,
		messageStorage: messageStorage,
		userManager:    userManager,
		log:            log,
	}
}

func (c *ChatService) Join(ctx context.Context, senderId, receiverId int64) (int64, error) {
	const op = "chat.Join"

	log := c.log.With(slog.String("op", op))

	log.Info("attempting to join chat")

	var patientId, doctorId int64

	if ctx.Value(middleware.CtxUserRole).(string) == "Patient" {
		patientId, doctorId = receiverId, senderId
	} else {
		patientId, doctorId = senderId, receiverId
	}

	session, err := c.sessionStorage.GetSessionByMembers(ctx, patientId, doctorId)
	if err != nil {
		if errors.Is(err, storage.ErrSessionNotFound) {
			sessionId, err := c.sessionStorage.CreateSession(ctx, patientId, doctorId)
			if err != nil {
				log.Error("failed to join chat", sl.Err(err))

				return 0, fmt.Errorf("%s: %w", op, err)
			}
			log.Info("user joined")

			return sessionId, nil
		}

		log.Error("failed to join chat", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("user joined")

	return session.ID, nil
}

func (c *ChatService) Save(ctx context.Context, message models.Message) (int64, error) {
	const op = "chat.Save"

	log := c.log.With(slog.String("op", op))

	log.Info("attempting to save message")

	messageId, err := c.messageStorage.SaveMessage(ctx, message)
	if err != nil {
		log.Error("failed to save message", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("message saved")

	return messageId, nil
}

func (c *ChatService) GetSessionIdByMembers(ctx context.Context, senderId, receiverId int64) (int64, error) {
	const op = "chat.GetSessionIdByMembers"

	log := c.log.With(slog.String("op", op))

	log.Info("attempting to get session")

	var patientId, doctorId int64

	if ctx.Value(middleware.CtxUserRole).(string) == "Patient" {
		patientId, doctorId = senderId, receiverId
	} else {
		patientId, doctorId = receiverId, senderId
	}

	session, err := c.sessionStorage.GetSessionByMembers(ctx, patientId, doctorId)
	if err != nil {
		log.Error("failed to get session", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	return session.ID, nil
}
