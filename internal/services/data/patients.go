package data

import (
	"context"
	"fmt"
	"log/slog"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/lib/logger/sl"
)

func (d *DataService) GetPatientByEmail(ctx context.Context, email string) (models.User, error) {
	const op = "data.GetPatientByEmail"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get patient by email")

	patient, err := d.patientStorage.GetPatientByEmail(ctx, email)
	if err != nil {
		log.Error("failed to get patient", sl.Err(err))

		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("patient received")

	return patient, nil
}

func (d *DataService) GetPatientByID(ctx context.Context, id int64) (models.User, error) {
	const op = "data.GetPatientByID"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get patient by id")

	patient, err := d.patientStorage.GetPatientById(ctx, id)
	if err != nil {
		log.Error("failed to get patient", sl.Err(err))

		return models.User{}, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("patient received")

	return patient, nil
}

func (d *DataService) GetPatientPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error) {
	const op = "data.GetPatientPrescriptions"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get patient prescriptions")

	prescriptions, err := d.patientStorage.GetPatientPrescriptions(ctx, id)
	if err != nil {
		log.Error("failed to get patient prescriptions", sl.Err(err))

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("patient prescriptions received")

	return prescriptions, nil
}

func (d *DataService) GetPatientDoctors(ctx context.Context, id int64) ([]models.Doctor, error) {
	const op = "data.GetPatientDoctors"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get patient doctors")

	doctors, err := d.patientStorage.GetPatientDoctors(ctx, id)
	if err != nil {
		log.Error("failed to get patient doctors", sl.Err(err))

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("patient doctors received")

	return doctors, nil
}

func (d *DataService) DeletePatient(ctx context.Context, id int64) error {
	const op = "data.DeletePatient"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to delete patient")

	err := d.patientStorage.DeletePatient(ctx, id)
	if err != nil {
		log.Error("failed to delete patient", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("patient deleted")

	return nil
}
