package data

import (
	"context"
	"log/slog"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
)

type PatientStorage interface {
	GetPatientByEmail(ctx context.Context, email string) (models.User, error)
	GetPatientById(ctx context.Context, id int64) (models.User, error)
	GetPatientPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error)
	GetPatientDoctors(ctx context.Context, id int64) ([]models.Doctor, error)
	DeletePatient(ctx context.Context, id int64) error
}

type DoctorStorage interface {
	GetDoctorByEmail(ctx context.Context, email string) (models.Doctor, error)
	GetDoctorById(ctx context.Context, id int64) (models.Doctor, error)
	GetDoctorPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error)
	GetDoctorPatients(ctx context.Context, id int64) ([]models.User, error)
	DeleteDoctor(ctx context.Context, id int64) error
}

type PrescriptionStorage interface {
	SavePrescription(ctx context.Context, prescription models.Prescription) (int64, error)
	GetPrescriptionById(ctx context.Context, id int64) (models.Prescription, error)
	GetPrescriptionsByBoth(ctx context.Context, patientId, doctorId int64) ([]models.Prescription, error)
	DeletePrescription(ctx context.Context, id int64) error
	UpdatePrescription(ctx context.Context, id int64, prescription models.PrescriptionUpdate) error
}

type DataService struct {
	patientStorage      PatientStorage
	doctorStorage       DoctorStorage
	prescriptionStorage PrescriptionStorage

	log *slog.Logger
}

func New(patientStorage PatientStorage, doctorStorage DoctorStorage, prescriptionStorage PrescriptionStorage, log *slog.Logger) *DataService {
	return &DataService{
		patientStorage:      patientStorage,
		doctorStorage:       doctorStorage,
		prescriptionStorage: prescriptionStorage,
		log:                 log,
	}
}
