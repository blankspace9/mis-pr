package data

import (
	"context"
	"fmt"
	"log/slog"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/lib/logger/sl"
	"gitlab.com/blankspace9/mis-pr/internal/services"
)

func (d *DataService) AddPrescription(ctx context.Context, prescription models.Prescription) (int64, error) {
	const op = "data.AddPrescription"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to add prescription")

	patient, err := d.patientStorage.GetPatientById(ctx, prescription.PatientID)
	if err != nil {
		log.Error("failed to get patient info", sl.Err(services.ErrInvalidUserID))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	if patient.Role != "Patient" {
		log.Error("patient is not a patient", sl.Err(services.ErrInvalidUserID))

		return 0, fmt.Errorf("%s: %w", op, services.ErrInvalidUserID)
	}

	doctor, err := d.patientStorage.GetPatientById(ctx, prescription.DoctorID)
	if err != nil {
		log.Error("failed to get doctor info", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	if doctor.Role != "Doctor" {
		log.Error("doctor is not a doctor", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, services.ErrInvalidUserID)
	}

	id, err := d.prescriptionStorage.SavePrescription(ctx, prescription)
	if err != nil {
		log.Error("failed to add prescription", sl.Err(err))

		return 0, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("prescription added")

	return id, nil
}

func (d *DataService) GetPrescriptionByID(ctx context.Context, id int64) (models.Prescription, error) {
	const op = "data.GetPrescriptionByID"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get prescription by id")

	prescription, err := d.prescriptionStorage.GetPrescriptionById(ctx, id)
	if err != nil {
		log.Error("failed to get prescription", sl.Err(err))

		return models.Prescription{}, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("prescription received")

	return prescription, nil
}

func (d *DataService) GetPrescriptionsByBoth(ctx context.Context, patientId, doctorId int64) ([]models.Prescription, error) {
	const op = "data.GetPrescriptionByBoth"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get prescriptions by both")

	prescriptions, err := d.prescriptionStorage.GetPrescriptionsByBoth(ctx, patientId, doctorId)
	if err != nil {
		log.Error("failed to get prescriptions", sl.Err(err))

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("prescriptions received")

	return prescriptions, nil
}

func (d *DataService) DeletePrescription(ctx context.Context, id int64) error {
	const op = "data.DeletePrescription"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to delete prescription")

	err := d.prescriptionStorage.DeletePrescription(ctx, id)
	if err != nil {
		log.Error("failed to delete prescription", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("prescription deleted")

	return nil
}

func (d *DataService) UpdatePrescription(ctx context.Context, id int64, prescription models.PrescriptionUpdate) error {
	const op = "data.UpdatePrescription"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to update prescription")

	err := d.prescriptionStorage.UpdatePrescription(ctx, id, prescription)
	if err != nil {
		log.Error("failed to update prescription", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("prescription updated")

	return nil
}
