package data

import (
	"context"
	"fmt"
	"log/slog"

	"gitlab.com/blankspace9/mis-pr/internal/domain/models"
	"gitlab.com/blankspace9/mis-pr/internal/lib/logger/sl"
)

func (d *DataService) GetDoctorByEmail(ctx context.Context, email string) (models.Doctor, error) {
	const op = "data.GetDoctorByEmail"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get doctor by email")

	doctor, err := d.doctorStorage.GetDoctorByEmail(ctx, email)
	if err != nil {
		log.Error("failed to get doctor", sl.Err(err))

		return models.Doctor{}, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("doctor received")

	return doctor, nil
}

func (d *DataService) GetDoctorByID(ctx context.Context, id int64) (models.Doctor, error) {
	const op = "data.GetDoctorByID"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get doctor by id")

	doctor, err := d.doctorStorage.GetDoctorById(ctx, id)
	if err != nil {
		log.Error("failed to get doctor", sl.Err(err))

		return models.Doctor{}, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("doctor received")

	return doctor, nil
}

func (d *DataService) GetDoctorPrescriptions(ctx context.Context, id int64) ([]models.Prescription, error) {
	const op = "data.GetDoctorPrescriptions"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get doctor prescriptions")

	prescriptions, err := d.doctorStorage.GetDoctorPrescriptions(ctx, id)
	if err != nil {
		log.Error("failed to get doctor prescriptions", sl.Err(err))

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("doctor prescriptions received")

	return prescriptions, nil
}

func (d *DataService) GetDoctorPatients(ctx context.Context, id int64) ([]models.User, error) {
	const op = "data.GetDoctorPatients"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to get doctor patients")

	patients, err := d.doctorStorage.GetDoctorPatients(ctx, id)
	if err != nil {
		log.Error("failed to get doctor patients", sl.Err(err))

		return nil, fmt.Errorf("%s: %w", op, err)
	}

	log.Info("doctor patients received")

	return patients, nil
}

func (d *DataService) DeleteDoctor(ctx context.Context, id int64) error {
	const op = "data.DeleteDoctor"

	log := d.log.With(slog.String("op", op))

	log.Info("attempting to delete doctor")

	err := d.doctorStorage.DeleteDoctor(ctx, id)
	if err != nil {
		log.Error("failed to delete doctor", sl.Err(err))

		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("doctor deleted")

	return nil
}
