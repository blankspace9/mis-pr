package services

import "errors"

var (
	ErrInvalidCredentials = errors.New("invalid credentials")
	ErrInvalidUserID      = errors.New("invalid user ID")
	ErrUserAlreadyExists  = errors.New("user already exists")
	ErrSessionNotFound    = errors.New("session not found")
)
