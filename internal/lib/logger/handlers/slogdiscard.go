package slogdiscard

import (
	"context"
	"log/slog"
)

type DiscardHandler struct{}

func NewDiscardHandler() *DiscardHandler {
	return &DiscardHandler{}
}

func (dh *DiscardHandler) Enabled(context.Context, slog.Level) bool {
	return false
}

func (dh *DiscardHandler) Handle(context.Context, slog.Record) error {
	return nil
}

func (dh *DiscardHandler) WithAttrs([]slog.Attr) slog.Handler {
	return dh
}

func (dh *DiscardHandler) WithGroup(string) slog.Handler {
	return dh
}
