package validation

import (
	"unicode"

	valid "github.com/asaskevich/govalidator"
)

func ValidatePassword(password string) bool {
	var special bool

	if len(password) < 8 || len(password) > 64 {
		return false
	}

	if !valid.IsASCII(password) {
		return false
	}

	if !valid.HasLowerCase(password) {
		return false
	}

	if !valid.HasUpperCase(password) {
		return false
	}

	if !valid.Matches(password, "[0-9]") {
		return false
	}

	for _, c := range password {
		if unicode.IsPunct(c) || unicode.IsSymbol(c) {
			special = true
			break
		}
	}

	return special
}
