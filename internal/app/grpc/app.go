package grpcapp

import (
	"fmt"
	"log/slog"
	"net"

	authgrpc "gitlab.com/blankspace9/mis-pr/internal/delivery/mis"
	"gitlab.com/blankspace9/mis-pr/internal/services/auth"
	"gitlab.com/blankspace9/mis-pr/internal/services/chat"
	"gitlab.com/blankspace9/mis-pr/internal/services/data"

	"google.golang.org/grpc"
)

type App struct {
	log        *slog.Logger
	gRPCServer *grpc.Server
	port       int
	api        *authgrpc.ServerAPI
}

// New creates new gRPC server app
func New(log *slog.Logger, authService *auth.Auth, dataService *data.DataService, chatService *chat.ChatService, port int) *App {
	gRPCServer, api := authgrpc.RegisterServerAPI(authService, dataService, dataService, dataService, chatService)

	return &App{
		log:        log,
		gRPCServer: gRPCServer,
		port:       port,
		api:        api,
	}
}

func (a *App) MustRun() {
	if err := a.Run(); err != nil {
		panic(err)
	}
}

func (a *App) Run() error {
	const op = "grpcapp.Run"

	log := a.log.With(slog.String("op", op), slog.Int("port", a.port))

	l, err := net.Listen("tcp", fmt.Sprintf(":%d", a.port))
	if err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	log.Info("gRPC server is running", slog.String("addr", l.Addr().String()))

	if err := a.gRPCServer.Serve(l); err != nil {
		return fmt.Errorf("%s: %w", op, err)
	}

	return nil
}

func (a *App) Stop() {
	const op = "grpcapp.Stop"

	a.log.With(slog.String("op", op)).Info("closing active connections", slog.Int("port", a.port))
	a.api.CloseActiveConnections()

	a.log.With(slog.String("op", op)).Info("stopping gRPC server", slog.Int("port", a.port))
	a.gRPCServer.GracefulStop()
}
