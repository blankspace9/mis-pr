package app

import (
	"log/slog"

	grpcapp "gitlab.com/blankspace9/mis-pr/internal/app/grpc"
	"gitlab.com/blankspace9/mis-pr/internal/config"
	"gitlab.com/blankspace9/mis-pr/internal/services/auth"
	"gitlab.com/blankspace9/mis-pr/internal/services/chat"
	"gitlab.com/blankspace9/mis-pr/internal/services/data"
	"gitlab.com/blankspace9/mis-pr/internal/storage/postgres"
)

type App struct {
	GRPCServer *grpcapp.App
}

func New(log *slog.Logger, cfg config.Config) *App {
	storage, err := postgres.New(postgres.PostgresConnectionInfo(cfg.Storage))
	if err != nil {
		panic(err)
	}

	authService := auth.New(storage, log, cfg.Auth.SecretKey, cfg.Auth.Tokens.AccessDuration)
	dataServce := data.New(storage, storage, storage, log)
	chatService := chat.New(storage, storage, storage, log)

	grpcApp := grpcapp.New(log, authService, dataServce, chatService, cfg.GRPC.Port)

	return &App{
		GRPCServer: grpcApp,
	}
}
