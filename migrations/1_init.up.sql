CREATE TYPE USER_ROLE AS ENUM ('Patient', 'Doctor', 'Admin');

CREATE TABLE IF NOT EXISTS users
(
    id SERIAL PRIMARY KEY,
    email TEXT NOT NULL UNIQUE,
    pass_hash TEXT NOT NULL,
    first_name VARCHAR(50) NOT NULL,
    last_name VARCHAR(50) NOT NULL,
    patronymic VARCHAR(50),
    phone_number VARCHAR(20) UNIQUE NOT NULL,
    role USER_ROLE NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE IF NOT EXISTS doctors
(
    user_id INTEGER PRIMARY KEY REFERENCES users(id) ON DELETE CASCADE,
    specialty VARCHAR(255),
    license VARCHAR(15)
);

CREATE TABLE IF NOT EXISTS prescriptions
(
    id SERIAL PRIMARY KEY,
    patient_id INTEGER REFERENCES users(id) ON DELETE SET NULL,
    doctor_id INTEGER REFERENCES users(id) ON DELETE SET NULL,
    date_of_issue DATE NOT NULL,
    duration_days INTEGER NOT NULL,
    title TEXT NOT NULL,
    dosage_mg INTEGER NOT NULL,
    way_to_use TEXT NOT NULL,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE TABLE IF NOT EXISTS sessions
(
    id SERIAL PRIMARY KEY,
    patient_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    doctor_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    created_at TIMESTAMP WITH TIME ZONE NOT NULL,
    CONSTRAINT unique_ids UNIQUE (patient_id, doctor_id)
);

CREATE TABLE IF NOT EXISTS messages
(
    id SERIAL PRIMARY KEY,
    session_id INTEGER REFERENCES sessions(id) ON DELETE CASCADE,
    sender_id INTEGER REFERENCES users(id) ON DELETE CASCADE,
    message TEXT NOT NULL,
    send_at TIMESTAMP WITH TIME ZONE NOT NULL
);

CREATE INDEX IF NOT EXISTS idx_users_email ON users (email);
CREATE INDEX IF NOT EXISTS idx_prescriptions_id ON prescriptions (id);
CREATE INDEX IF NOT EXISTS idx_prescriptions_patient_id ON prescriptions (patient_id);
CREATE INDEX IF NOT EXISTS idx_prescriptions_doctor_id ON prescriptions (doctor_id);
CREATE INDEX IF NOT EXISTS idx_messages_session_id ON messages (session_id);
CREATE INDEX IF NOT EXISTS idx_sessions_patient_id ON sessions (patient_id);
CREATE INDEX IF NOT EXISTS idx_sessions_doctor_id ON sessions (doctor_id);
CREATE INDEX IF NOT EXISTS idx_sessions_both_id ON sessions (patient_id, doctor_id);